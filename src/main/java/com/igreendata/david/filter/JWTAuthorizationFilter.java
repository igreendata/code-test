package com.igreendata.david.filter;

import com.igreendata.david.util.JWTTokenUtil;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static com.igreendata.david.util.JWTTokenUtil.*;

/**
 * Created by David.Zheng on 16/1/21.
 */
public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    public JWTAuthorizationFilter(AuthenticationManager authenticationManager) {
        super(authenticationManager);
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader(HEADER_HEADER);
        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            chain.doFilter(request, response);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(request);

        SecurityContextHolder.getContext().setAuthentication(authentication);
        chain.doFilter(request, response);
    }

    /**
     *
     * @param request
     * @return
     */
    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_HEADER);
        if (token != null) {
            token = token.replace(JWTTokenUtil.TOKEN_PREFIX, "");
            String username = JWTTokenUtil.getUsername(token);

            String[] authorities = JWTTokenUtil.getUserRole(token);
            Set<SimpleGrantedAuthority> grantedAuthorities = Arrays.stream(authorities)
                    .map(v -> new SimpleGrantedAuthority(v))
                    .collect(Collectors.toSet());

            if (username != null) {
                return new UsernamePasswordAuthenticationToken(username, null, grantedAuthorities);
            }
            return null;
        }
        return null;
    }

}
