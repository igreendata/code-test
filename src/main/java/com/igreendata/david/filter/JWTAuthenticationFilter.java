package com.igreendata.david.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.igreendata.david.entity.ApplicationUser;
import com.igreendata.david.util.JWTTokenUtil;
import lombok.SneakyThrows;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

import static com.igreendata.david.util.JWTTokenUtil.HEADER_HEADER;
import static com.igreendata.david.util.JWTTokenUtil.TOKEN_PREFIX;

/**
 * Created by David.Zheng on 16/1/21.
 */
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
    }

    /**
     * Authenticate the user, verify the user password
     *
     * @param request
     * @param response
     * @return
     * @throws AuthenticationException
     */
    @Override
    @SneakyThrows
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        ApplicationUser applicationUser = new ObjectMapper()
                .readValue(request.getInputStream(), ApplicationUser.class);

        return authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(applicationUser.getName(), applicationUser.getPassword()));
    }

    /**
     * return the jwt token if successful
     *
     * @param request
     * @param response
     * @param chain
     * @param auth
     * @throws IOException
     * @throws ServletException
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication auth) throws IOException, ServletException {
        User user = (User)auth.getPrincipal();
        Collection<GrantedAuthority> authorities = user.getAuthorities();
        String token = JWTTokenUtil.createToken(user.getUsername(), authorities.toString());

        response.addHeader(HEADER_HEADER, TOKEN_PREFIX + token);
    }

}
