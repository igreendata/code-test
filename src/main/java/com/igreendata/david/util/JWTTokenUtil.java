package com.igreendata.david.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by David.Zheng on 16/1/21.
 */
public class JWTTokenUtil {

    public static final int EXPIRATION_TIME = 600*1000;  //10 minutes

    private static final String SECRET = "SecretKeyToGenJWTs";

    public static final String HEADER_HEADER = "Authorization";

    public static final String TOKEN_PREFIX = "Bearer ";

    public static final String ROLE_CLAIMS = "role";

    public static String createToken(String username,String role) {
        Map<String,Object> map = new HashMap<>();
        map.put(ROLE_CLAIMS, role);

        String token = Jwts
                .builder()
                .setSubject(username)
                .setClaims(map)
                .claim("username",username)
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS256, SECRET).compact();
        return token;
    }

    /**
     * Verify the Token
     */
    public static Claims verifyJWT(String token) {
        try {
            final Claims claims = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody();
            return claims;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * get username from the token
     */
    public static String getUsername(String token){
        Claims claims = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody();
        return claims.get("username").toString();
    }

    /**
     * get roles from the token
     */
    public static String[] getUserRole(String token){
        Claims claims = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody();
        String roleString = claims.get("role").toString();
        return StringUtils.strip(roleString, "[]").split(", ");
    }

    /**
     * Check the token expiration
     */
    public static boolean isExpiration(String token){
        Claims claims = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody();
        return claims.getExpiration().before(new Date());
    }

}
