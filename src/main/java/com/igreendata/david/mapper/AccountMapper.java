package com.igreendata.david.mapper;

import com.igreendata.david.dto.AccountDto;
import com.igreendata.david.entity.Account;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * Created by David.Zheng on 16/1/21.
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AccountMapper {

    AccountDto entityToDTO(Account entity);

    Account DtoToEntity(AccountDto dto);
}
