package com.igreendata.david.mapper;

import com.igreendata.david.dto.TransactionDto;
import com.igreendata.david.entity.Transaction;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

/**
 * Created by David.Zheng on 16/1/21.
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface TransactionMapper {

    @Mapping(source = "id", target = "transactionId")
    TransactionDto entityToDto(Transaction entity);

}
