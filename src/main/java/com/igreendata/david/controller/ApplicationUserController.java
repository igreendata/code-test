package com.igreendata.david.controller;

import com.igreendata.david.entity.ApplicationUser;
import com.igreendata.david.repository.ApplicationUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by David.Zheng on 16/1/21.
 */
//@RestController
//@RequestMapping("/users")
public class ApplicationUserController {

    @Autowired
    private ApplicationUserRepo applicationUserRepo;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping("/sign-up")
    public void signUp(@RequestBody ApplicationUser user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        applicationUserRepo.save(user);
    }
}
