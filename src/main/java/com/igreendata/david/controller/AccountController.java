package com.igreendata.david.controller;

import com.igreendata.david.dto.AccountDto;
import com.igreendata.david.dto.TransactionDto;
import com.igreendata.david.service.AccountService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by David.Zheng on 15/1/21.
 */

@Log4j2
@RestController
@RequestMapping("/accounts")
public class AccountController {

    @Autowired
    private AccountService accountService;


    //--------------------------------------Admin Operations--------------------------------------

//    /**
//     * Get the Account By Id
//     *
//     * @param id
//     * @return
//     */
//    @GetMapping(value = "/{id}")
//    public ResponseEntity<AccountDto> getAccountByAccountId(@PathVariable("id") Long id) {
//        AccountDto accountDto = new AccountDto();
//        return ResponseEntity.ok(accountDto);
//    }

    /**
     * Lists all the accounts
     *
     * @return
     */
    @GetMapping
    @PreAuthorize("hasAuthority('account:read')")
    public ResponseEntity<?> listAccounts() {
        List<AccountDto> accountDtoList = accountService.listAccounts();
        return ResponseEntity.ok(accountDtoList);
    }

    /**
     * Get the transactions by account id
     *
     * @param accountId
     * @return
     */
    @GetMapping(value = "/{id}/transactions")
    @PreAuthorize("hasAuthority('transaction:read')")
    public ResponseEntity<?> getTransactionsByAccountId(@PathVariable("id") Long accountId) {
        List<TransactionDto> transactionDtoList = accountService.listTransactionByAccountId(accountId);
        return ResponseEntity.ok(transactionDtoList);
    }


}
