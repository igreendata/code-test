package com.igreendata.david.service;

import com.igreendata.david.dto.AccountDto;
import com.igreendata.david.dto.TransactionDto;
import com.igreendata.david.mapper.AccountMapper;
import com.igreendata.david.mapper.TransactionMapper;
import com.igreendata.david.repository.AccountRepo;
import com.igreendata.david.repository.TransactionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by David.Zheng on 15/1/21.
 */
@Service
public class AccountService {

    @Autowired
    private AccountRepo accountRepo;

    @Autowired
    private TransactionRepo transactionRepo;

    @Autowired
    private AccountMapper accountMapper;

    @Autowired
    private TransactionMapper transactionMapper;

    /**
     * List the Accounts and converts them to Account DTO
     *
     * @return
     */
    public List<AccountDto> listAccounts() {
        return accountRepo.findAll().stream()
                .map(account -> accountMapper.entityToDTO(account))
                .collect(Collectors.toList());
    }

    /**
     * Lists the transaction by the accountId
     *
     * @param accountId
     * @return
     */
    public List<TransactionDto> listTransactionByAccountId(long accountId) {
        return transactionRepo.findAllByAccountId(accountId).stream()
                .map(transaction -> transactionMapper.entityToDto(transaction))
                .collect(Collectors.toList());
    }


}
