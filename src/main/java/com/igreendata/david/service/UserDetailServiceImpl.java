package com.igreendata.david.service;

import com.igreendata.david.entity.ApplicationUser;
import com.igreendata.david.repository.ApplicationUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by David.Zheng on 16/1/21.
 */
@Service("UserDetailsService")
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private ApplicationUserRepo applicationUserRepo;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        ApplicationUser applicationUser = applicationUserRepo.findByUsername(s)
                .orElseThrow(
                        () -> new UsernameNotFoundException("ApplicationUser doesn't exist"));

        List<SimpleGrantedAuthority> authorities = new ArrayList<>();
        applicationUser.getPermissions().forEach(p-> {
            authorities.add(new SimpleGrantedAuthority(p.getName()));
        });

        return new User(applicationUser.getName(), applicationUser.getPassword(), authorities);
    }
}
