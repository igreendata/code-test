package com.igreendata.david.dto;

import com.igreendata.david.constant.TransferType;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by David.Zheng on 15/1/21.
 */
@Data
public class TransactionDto {

    private long transactionId;

    private String currency;

    private BigDecimal amount;

    private TransferType transferType;

    private String desc;

    private LocalDateTime transactionTime;
}
