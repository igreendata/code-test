package com.igreendata.david.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by David.Zheng on 15/1/21.
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AccountDto {

    @JsonIgnore
    private long id;

    private String accountName;

    private String accountNumber;

    private String accountType;

    private LocalDateTime balanceDate;

    private BigDecimal balance;

    private LocalDateTime openDate;

}
