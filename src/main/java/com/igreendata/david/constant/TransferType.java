package com.igreendata.david.constant;

/**
 * Created by David.Zheng on 15/1/21.
 */
public enum TransferType {
    Debit,
    Credit
}
