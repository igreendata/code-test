package com.igreendata.david.constant;

/**
 * Created by David.Zheng on 16/1/21.
 */
public enum AccountType {

    Debit(0),

    Credit(1),

    Joint(2);

    public final int type;

    AccountType(int type) {
        this.type = type;
    }


}
