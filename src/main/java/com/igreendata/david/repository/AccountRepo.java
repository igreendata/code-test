package com.igreendata.david.repository;

import com.igreendata.david.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by David.Zheng on 16/1/21.
 */
@Repository
public interface AccountRepo extends JpaRepository<Account, Long> {

}
