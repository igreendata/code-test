package com.igreendata.david.repository;

import com.igreendata.david.entity.ApplicationUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by David.Zheng on 16/1/21.
 */
@Repository
public interface ApplicationUserRepo extends JpaRepository<ApplicationUser, Long> {

    @Query("select u from ApplicationUser u join fetch u.permissions where u.name=:username")
    Optional<ApplicationUser> findByUsername(String username);
}
