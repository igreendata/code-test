package com.igreendata.david.repository;

import com.igreendata.david.entity.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by David.Zheng on 16/1/21.
 */
@Repository
public interface TransactionRepo extends JpaRepository<Transaction, Long> {

    List<Transaction> findAllByAccountId(long accountId);
}
