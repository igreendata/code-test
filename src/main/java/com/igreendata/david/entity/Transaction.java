package com.igreendata.david.entity;

import com.igreendata.david.constant.TransferType;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * Created by David.Zheng on 15/1/21.
 */
@Data
@Entity
@Table(name = "ACCOUNT_TRANSACTION")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private long id;

    @Column(name = "CURRENCY", nullable = false)
    private String currency;

    @Column(name = "AMOUNT", nullable = false)
    private BigDecimal amount;

    @Column(name = "TRX_TYPE")
    @Enumerated(EnumType.STRING)
    private TransferType transferType;

    @Column(name = "TRX_DESC")
    private String desc;

    @Column(name = "TRX_TIME")
    private LocalDateTime transactionTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ACCOUNT_ID", nullable = false)
    private Account account;

}
