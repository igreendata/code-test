package com.igreendata.david.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * Created by David.Zheng on 15/1/21.
 */
@Data
@Entity
@Table(name = "ACCOUNT")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", updatable = false, nullable = false)
    private long id;

    @Column(name = "ACCOUNT_NAME", updatable = false, nullable = false)
    private String accountName;

    @Column(name = "ACCOUNT_NUMBER", updatable = false, nullable = false)
    private String accountNumber;

    @Column(name = "ACCOUNT_TYPE", updatable = false, nullable = false)
    private String accountType;

    @Column(name = "BALANCE")
    private BigDecimal balance;

    @Column(name = "BALANCE_DATE")
    private LocalDateTime balanceDate;

    @Column(name = "OPEN_DATE", updatable = false, nullable = false)
    private LocalDateTime openDate;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
    private Set<Transaction> transactions;

}
