package com.igreendata.david.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by David.Zheng on 16/1/21.
 */
@Data
@Entity
@Table(name="ACL_USER")
public class ApplicationUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "PASSWORD", nullable = false)
    private String password;

    @Column(name = "LAST_UPDATE")
    private LocalDateTime lastUpdate =  LocalDateTime.now();

    @ManyToMany
    @JoinTable(name="acl_user_permission",
            joinColumns = @JoinColumn(name="user_id"),
    inverseJoinColumns = @JoinColumn(name="permission_id"))
    private List<Permission> permissions;

}
