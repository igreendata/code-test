package com.igreendata.david.entity;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

/**
 * Created by David.Zheng on 16/1/21.
 */
@Data
@Entity
@ToString(exclude = "applicationUsers")
@Table(name = "ACL_PERMISSION")
public class Permission {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @ManyToMany(mappedBy = "permissions")
//    @JoinTable(name="acl_user_permissions",
//    joinColumns = @JoinColumn(name="permission_id"),
//    inverseJoinColumns = @JoinColumn(name="user_id"))
    private List<ApplicationUser> applicationUsers;

}
