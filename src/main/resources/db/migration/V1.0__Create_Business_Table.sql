-- igreendata.account definition

CREATE TABLE `account` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_name` varchar(255) NOT NULL,
  `account_number` varchar(255) NOT NULL,
  `account_type` varchar(255) NOT NULL,
  `balance` decimal(19,2) DEFAULT NULL,
  `balance_date` datetime DEFAULT NULL,
  `open_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- igreendata.account_transaction definition

CREATE TABLE `account_transaction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_id` bigint(20) NOT NULL,
  `amount` decimal(19,2) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `trx_type` varchar(255) DEFAULT NULL,
  `trx_time` datetime DEFAULT NULL,
  `trx_desc` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKqonh25s0w6r5cf8jq88m6kd8o` (`account_id`),
  CONSTRAINT `FKqonh25s0w6r5cf8jq88m6kd8o` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;