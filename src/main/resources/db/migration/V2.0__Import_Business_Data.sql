INSERT INTO account (account_name,account_number,account_type,balance,balance_date,open_date) VALUES
('MR David','111111111','Debit',1.00,'2021-01-16 10:44:27.0','2021-01-16 10:44:27.0')
,('MR Jeffery Test','222222222','Credit',111.00,'2021-01-16 10:44:27.0','2021-01-16 10:44:27.0');

INSERT INTO account_transaction (amount,currency,trx_desc,trx_time,trx_type,account_id) VALUES
(1.00,'AUD','pocket money','2021-01-16 10:44:27.0','Debit',1)
,(88.00,'AUD','lottery','2021-01-16 10:44:27.0','Credit',1)
,(100.00,'USD','internaltional transfer','2021-01-16 10:44:27.0','Debit',2);