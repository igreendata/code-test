INSERT INTO acl_user (last_update,name,password) VALUES
('2021-01-17 00:15:29.0','admin','$2a$10$ztnjhAvLmbzcUvrlP/Qwv.qaHqb8uB5ixAwwBIN6oshayUifFNGtq')
,('2021-01-17 10:11:09.0','david','$2a$10$YbEHuwh/ORO7CHy9bnAo7.Rzll5QfHoNENAhXLLqyi2.nwAW82iKi');

insert into acl_permission (name) values('account:read'),('transaction:read');


insert into acl_user_permission(user_id, permission_id)
select acl_user.id, acl_permission.id from acl_user, acl_permission
where acl_user.name='admin' and acl_permission.name='account:read';

insert into acl_user_permission(user_id, permission_id)
select acl_user.id, acl_permission.id from acl_user, acl_permission
where acl_user.name='admin' and acl_permission.name='transaction:read';


insert into acl_user_permission(user_id, permission_id)
select acl_user.id, acl_permission.id from acl_user, acl_permission
where acl_user.name='david' and acl_permission.name='account:read';

