package com.igreendata.david.service;

import com.igreendata.david.constant.TransferType;
import com.igreendata.david.dto.AccountDto;
import com.igreendata.david.dto.TransactionDto;
import com.igreendata.david.entity.Account;
import com.igreendata.david.entity.Transaction;
import com.igreendata.david.repository.AccountRepo;
import com.igreendata.david.repository.TransactionRepo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

/**
 * Created by David.Zheng on 17/1/21.
 */
@SpringBootTest
class AccountServiceTest {

    @Autowired
    private AccountService accountService;

    @MockBean
    private AccountRepo accountRepo;

    @MockBean
    private TransactionRepo transactionRepo;

    @Test
    void listAccounts() {
        initAccountData();
        List<AccountDto> accountDtos = accountService.listAccounts();
        assertNotNull(accountDtos);
        assertEquals(1, accountDtos.size());
    }

    @Test
    void listTransactionByAccountId() {
        initTransactionData();
        List<TransactionDto> transactionDtos = accountService.listTransactionByAccountId(1);
        assertNotNull(transactionDtos);
        assertEquals(1, transactionDtos.size());
    }

    private void initTransactionData() {
        Transaction transaction = new Transaction();
        transaction.setTransactionTime(LocalDateTime.now());
        transaction.setAmount(BigDecimal.valueOf(100));
        transaction.setTransferType(TransferType.Credit);
        transaction.setCurrency("AUD");
        transaction.setId(1);
        List<Transaction> list = new ArrayList<>();
        list.add(transaction);

        when(transactionRepo.findAllByAccountId(1)).thenReturn(list);
    }

    private void initAccountData() {
        Account account = new Account();
        account.setAccountName("test");
        account.setAccountType("Credit");
        account.setBalance(BigDecimal.valueOf(99.99));
        List<Account> list = new ArrayList<>();
        list.add(account);
        when(accountRepo.findAll()).thenReturn(list);
    }
}