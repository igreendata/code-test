package com.igreendata.david.controller;

import com.igreendata.david.constant.TransferType;
import com.igreendata.david.dto.AccountDto;
import com.igreendata.david.dto.TransactionDto;
import com.igreendata.david.service.AccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by David.Zheng on 17/1/21.
 */
@WebMvcTest(AccountController.class)
class AccountControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AccountService accountService;


    @BeforeEach
    public void initData() {
        AccountDto account = new AccountDto();
        account.setAccountName("test");
        account.setAccountType("Credit");
        account.setBalance(BigDecimal.valueOf(99.99));
        List<AccountDto> list = new ArrayList<>();
        list.add(account);


        when(accountService.listTransactionByAccountId(1)).thenReturn(initTransaction());
        when(accountService.listTransactionByAccountId(2)).thenReturn(initTransaction());
        when(accountService.listAccounts()).thenReturn(list);
    }

    @Test
    @WithMockUser(authorities = {"account:read"})
    void listAccounts() throws Exception {
        mockMvc.perform(get("/accounts"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0]").exists())
                .andExpect(jsonPath("$.[0].accountName").value("test"))
                .andExpect(jsonPath("$.[0].balance").value("99.99"));
    }


    @ParameterizedTest
    @ValueSource(longs = {1, 2})
    @WithMockUser(authorities = {"transaction:read"})
    void getTransactionsByAccountId(long accountId) throws Exception {
        mockMvc.perform(get("/accounts/" + accountId + "/transactions"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0]").exists())
                .andExpect(jsonPath("$.[0].currency").value("AUD"))
                .andExpect(jsonPath("$.[0].transferType").value("Credit"))
                .andExpect(jsonPath("$.[0].amount").value(100));

    }

    private List initTransaction() {
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setTransactionId(1);
        transactionDto.setAmount(BigDecimal.valueOf(100));
        transactionDto.setCurrency("AUD");
        transactionDto.setTransferType(TransferType.Credit);
        List<TransactionDto> list = new ArrayList<>();
        list.add(transactionDto);

        return list;
    }


}