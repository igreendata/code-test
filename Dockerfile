#mkdir -p target/dependency && (cd target/dependency; jar -xf ../*.jar)


FROM adoptopenjdk:8-jdk-hotspot
MAINTAINER David.Zheng <davidzheng1022@gmail.com>
VOLUME /tmp
ARG DEPENDENCY=target/dependency
COPY ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY ${DEPENDENCY}/META-INF /app/META-INF
COPY ${DEPENDENCY}/BOOT-INF/classes /app
COPY scripts/start.sh /app
RUN chmod +x /app/start.sh

RUN chown -R 1001:0 /app
USER 1001

EXPOSE 8080
CMD ["sh", "-c", "/app/start.sh"]