# IGreenData Springboot application


## Run this application

* Download the code and then go to project folder
* Run the command to build&start the project: **mvn spring-boot:run -Dspring-boot.run.profiles=dev**

## Test the application

* Two users with default permissions created by default (**david [account:read]**, **admin [account:read, transaction:read]**)

* Run the curl to gain the access token, change the name to see different authorization

  ~~~bash
  curl -i -H "Content-Type: application/json" -X POST -d '{ "name": "admin","password": "password"}' http://localhost:8080/login
  ~~~

* Open swagger page http://localhost:8080/swagger-ui/ and paste the token from last step

  >
  >For example: using id with 1, the admin would see the results, the user david would be blocked.

# Tech Stack

* **Springboot** quick development
* **Spring security** protect your application with confidence
* **Swagger** easy to view/test rest api
* **Flyway** manage the db scripts
* **Docker** build as docker image for future deployment
* **Gitlab CICD** my faviorte, lots of out of box features, much better than jenkins.

## Next Step

* Create roles instread of permission directly attached to users
* Remove the hardcoded jwt secret
* CICD Pipeline, need to build your own image builder(maven+jdk+docker) to walk around the paid feature.
* ......

